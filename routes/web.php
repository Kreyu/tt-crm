<?php

Route::get('/', function () {
    return redirect('dashboard');
});

// Dashboard
Route::get('dashboard', 'Web\DashboardController@dashboard')->name('dashboard');
Route::get('profile', 'Web\DashboardController@profile')->name('profile');

// Auth
Route::get('login', 'Web\Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Web\Auth\LoginController@login');
Route::post('logout', 'Web\Auth\LoginController@logout')->name('logout');
