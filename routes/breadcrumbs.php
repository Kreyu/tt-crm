<?php

Breadcrumbs::for('dashboard', function ($trail) {
    $trail->push('Dashboard', route('dashboard'), [
        'icon' => 'fa fas fa-home',
        'translation' => 'dashboard.sidebar.dashboard'
    ]);
});

Breadcrumbs::for('profile', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('My profile', route('profile'), [
        'translation' => 'dashboard.sidebar.profile'
    ]);
});
