<?php

use Illuminate\Http\Request;

// Users
Route::get('/users', 'Api\UserController@index');
Route::get('/users/{user}', 'Api\UserController@show');

// Comments
Route::get('/comments', 'Api\CommentController@index');
Route::get('/comments/{comment}', 'Api\CommentController@show');

// Issues
Route::get('/issues', 'Api\IssueController@index');
Route::get('/issues/{issue}', 'Api\IssueController@show');

// Timesheets
Route::get('/timesheets', 'Api\TimesheetController@index');
Route::get('/timesheets/{timesheet}', 'Api\TimesheetController@show');

// Projects
Route::get('/projects', 'Api\ProjectController@index');
Route::get('/projects/{project}', 'Api\ProjectController@show');

Route::middleware(['web', 'auth.api'])->group(function () {
    Route::get('/worktime', 'Api\UserController@worktime');
    Route::post('/sign', 'Api\UserController@sign');
});
