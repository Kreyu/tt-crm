import axios from 'axios'
import Api from './../misc/Api'
import Lang from './../misc/Lang'

/**
 * Timer class
 *
 * This class manages frontend work timer.
 *
 * @author Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
export default class Timer {

    /** Running state */
    public _isRunning: boolean;

    /** Time elapsed */
    public _elapsed: number;

    /** Interval instance */
    private interval: any;

    /** Timer DOM element */
    private timer: HTMLElement;

    /** Timer value DOM element */
    private timerValue: HTMLElement;

    /** Timer raw value DOM element */
    private timerValueRaw: HTMLElement;

    /** Sidebar's account status element */
    private accountStatus: HTMLElement;

    /** Sidebar's account status raw */
    private accountStatusRaw: HTMLElement;

    /** Sidebar's account status icon */
    private accountStatusIcon: HTMLElement;

    /** Sidebar's account status label */
    private accountStatusLabel: HTMLElement;

    /** Create Timer class */
    public constructor() {
        this.createHTMLElements()

        this._elapsed = this.getTimerRawValue()
        this._isRunning = false
        this.interval = null
    }

    /** Create HTML elements for later manipulation */
    private createHTMLElements() {
        this.timer = <HTMLElement> document.getElementById('timer')
        this.timerValue = <HTMLElement> this.timer.querySelector('#timer__value')
        this.timerValueRaw = <HTMLElement> this.timer.querySelector('#timer__value--raw')

        this.accountStatus = <HTMLElement> document.getElementById('account-status')
        this.accountStatusRaw = <HTMLElement> this.accountStatus.querySelector('#account-status--raw')
        this.accountStatusIcon = <HTMLElement> this.accountStatus.querySelector('#account-status__icon')
        this.accountStatusLabel = <HTMLElement> this.accountStatus.querySelector('#account-status__label')
    }

    /** Get raw value of timer */
    private getTimerRawValue(): number {
        return parseInt(this.timerValueRaw.innerHTML)
    }

    /** Get raw account state */
    private getAccountStatusRaw(): boolean {
        return !!+parseInt(this.accountStatusRaw.innerHTML);
    }

    /** Get isRunning state */
    get isRunning(): boolean {
        return this._isRunning;
    }

    /** Get time elapsed in seconds */
    get timeElapsed(): number {
        return this._elapsed;
    }

    /** Initialize counter, bind onclick methods */
    public initialize(): void {
        if (this.getAccountStatusRaw()) {
            this.toggleInterval()
            this.updateFrontend()
        }

        this.timer.onclick = () => {
            this.toggleInterval()
            this.updateFrontend()
            Api.sign()
        }
    }

    /** Toggle interval, depending on isRunning property */
    private toggleInterval(): void {
        this._isRunning ?
            this.pause() :
            this.start()
    }

    /** Start interval, set running state to true */
    private start(): void {
        this.interval = setInterval(() => this.tick(), 1000);
        this._isRunning = true
    }

    /** Clear interval, set running state to false */
    private pause(): void {
        clearInterval(this.interval)
        this._isRunning = false
    }

    /** Function to run every second when timer is active */
    private tick(): void {
        this._elapsed++
        this.timerValue.innerHTML = new Date(this._elapsed * 1000)
            .toISOString().substr(11, 8)
    }

    /** Update user account status on frontend */
    private updateFrontend(): void {
        this._isRunning ?
            this.setWorking() :
            this.setOnline()
    }

    /** Set account status to "working" */
    private setWorking(): void {
        this.updateHTMLElements('warning', 'working')
    }

    /** Set account status to "online" (idle) */
    private setOnline(): void {
        this.updateHTMLElements('success', 'online')
    }

    /** Update status icon class and label text */
    private updateHTMLElements(icon: string, label: string): void {
        this.accountStatusIcon.classList.remove('text-warning', 'text-success')
        this.accountStatusIcon.classList.add('text-' + icon)
        this.accountStatusLabel.innerHTML = Lang.get('dashboard.sidebar.account.status.' + label)
    }
}
