// @ts-ignore
import Lang from 'lang.js'
import * as messages from './../../../lang/lang.json'

export default new Lang({
    fallback: 'en',
    messages: messages
})
