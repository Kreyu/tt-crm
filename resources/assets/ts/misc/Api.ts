import axios, { AxiosInstance, AxiosPromise } from 'axios'

export default class Api {

    private static _baseURL: string = '/api/'

    private static _requests: Array<AxiosPromise> = []

    private static _headers: object = {
        'X-Requested-With': 'XMLHttpRequest',
        'X-CSRF-TOKEN': (<any>window).$csrfToken
    }

    private static _params: string = '?token=' + (<any>window).$apiToken

    private static _instance: AxiosInstance = axios.create({
        baseURL: Api._baseURL,
        headers: Api._headers
    })

    public static async sign(): Promise<any> {
        return await Api._instance.post(
            Api.linkto('sign')
        )
    }

    private static linkto(endpoint: string): string {
        return endpoint + Api._params;
    }
}
