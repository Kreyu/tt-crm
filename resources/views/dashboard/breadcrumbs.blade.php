@if (count($breadcrumbs))

    <ol class="breadcrumb">
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$loop->last)
                <li>
                    @if ($icon = optional($breadcrumb)->icon)
                        <i class="{{ $icon }}"></i>
                    @endif

                    <a href="{{ $breadcrumb->url }}">
                        {{ __($breadcrumb->translation) }}
                    </a>
                </li>
            @else
                <li class="active">
                    @if ($icon = optional($breadcrumb)->icon)
                        <i class="{{ $icon }}"></i>
                    @endif

                    {{ __($breadcrumb->translation) }}
                </li>
            @endif

        @endforeach
    </ol>

@endif
