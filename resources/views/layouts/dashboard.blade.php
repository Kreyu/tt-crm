<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Glorious title -->
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"
        integrity="sha256-916EbMg70RQy9LHiGkXzG8hSg9EdNy97GazNG/aiY1w="
        crossorigin="anonymous" />

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
        integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
        crossorigin="anonymous" />

    <!-- AdminLTE -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/css/AdminLTE.min.css"
        integrity="sha256-7WqktPHVsROEjpu4RjXZv4E5ZHZ3HgNOVxuYSxF6Bj4="
        crossorigin="anonymous" />

    <!-- AdminLTE Skin -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/css/skins/skin-blue.css"
        integrity="sha256-G+aMGT+eWxsu79kgmky5ys4zaMYEFI02AGtKM/N1oD0="
        crossorigin="anonymous" />

    <!-- CRM Stylesheets -->
    <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}">

    @yield('stylesheets')
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <!-- Navbar section -->
        <header class="main-header">

            <!-- Logo -->
            <a href="{{ route('dashboard') }}" class="logo">
                <span class="logo-mini"><b>A</b>LT</span>
                <span class="logo-lg"><b>Admin</b>LTE</span>
            </a>

            <!-- Navbar -->
            @include('layouts.dashboard.navbar')
        </header>

        <!-- Sidebar section -->
        <aside class="main-sidebar">

            <!-- Sidebar -->
            @include('layouts.dashboard.sidebar')
        </aside>

        <!-- Content section -->
        <div class="content-wrapper">

            <!-- Content header -->
            <section class="content-header">
                <h1>{{ Breadcrumbs::current()->title }}</h1>

                {{ Breadcrumbs::render() }}
            </section>

            <!-- Page content -->
            @yield('content')
        </div>

        <!-- Footer section -->
        <footer class="main-footer">

            <!-- Website version -->
            <div class="pull-right hidden-xs">
                <b>{{ __('dashboard.footer.version') }}</b> 1.0.0
            </div>

            <!-- Copyright stuff -->
            <strong>
                Made with <i class="fa fa-heart"></i> by
                <a href="https://swroblewski.pl" target="_blank">
                    Sebastian Wróblewski</a>
            </strong>
            | 2018
        </footer>
    </div>

    <!-- jQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>

    <!-- Bootstrap -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha256-U5ZEeKfGNOja007MMD3YBI0A3OSZOQbeG6z2f2Y0hu8="
        crossorigin="anonymous"></script>

    <!-- FastClick -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js"
        integrity="sha256-t6SrqvTQmKoGgi5LOl0AUy+lBRtIvEJ+++pLAsfAjWs="
        crossorigin="anonymous"></script>

    <!-- AdminLTE App -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/js/adminlte.min.js"
        integrity="sha256-Q/aizhMDU+m2KQAJOfeYLCMrkltKkaNkvdGr2bcxA74="
        crossorigin="anonymous"></script>

    <!-- CRM Scripts -->
    <script>
        window.$csrfToken = '{!! csrf_token() !!}'
        window.$apiToken = '{!! Auth::user()->api_token !!}'
    </script>
    <script src="{{ mix('js/dashboard.js') }}"></script>

    @yield('scripts')
</body>

</html>
