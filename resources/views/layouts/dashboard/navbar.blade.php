<nav class="navbar navbar-static-top">

    <!-- Sidebar toggle button -->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <i class="fas fa-bars"></i>
    </a>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

            <!-- Timer -->
            @include('layouts.dashboard.navbar.timer')

            <!-- Notifications -->
            @include('layouts.dashboard.navbar.notifications')

            <!-- Issues -->
            @include('layouts.dashboard.navbar.issues')

            <!-- User account -->
            @include('layouts.dashboard.navbar.account')
        </ul>
    </div>
</nav>
