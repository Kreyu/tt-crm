<div class="user-panel">
    <div class="pull-left image">
        <a href="{{ route('profile') }}">
            <img src="{{ asset($avatar) }}" class="img-circle" />
        </a>
    </div>
    <div class="pull-left info">
        <p>
            <a href="{{ route('profile') }}">
                {{ Auth::user()->name }}
            </a>
        </p>

        <div id="account-status">
            <i class="fa fa-circle text-{{ Auth::user()->isWorking() ? 'warning' : 'success' }}"
                id="account-status__icon"></i>

            <span id="account-status__label">
                {{ __('dashboard.sidebar.account.status.' .
                    (Auth::user()->isWorking() ? 'working' : 'online')) }}
            </span>

            <span id="account-status--raw">
                {{ (int) Auth::user()->isWorking() }}
            </span>
        </div>
    </div>
</div>
