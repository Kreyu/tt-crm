<section class="sidebar">

    <!-- User panel -->
    @include('layouts.dashboard.sidebar.account')

    <!-- Search form -->
    @include('layouts.dashboard.sidebar.search')

    <!-- Tree menu -->
    @include('layouts.dashboard.sidebar.tree')
</section>
