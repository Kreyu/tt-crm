<li class="dropdown tasks-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="far fa-flag"></i>
        @if ($issueCount = count($issues))
            <span class="label label-danger">{{ $issueCount }}</span>
        @endif
    </a>
    <ul class="dropdown-menu">
        <li class="header">{{ __('dashboard.navbar.issues.header', ['quantity' => $issueCount]) }}</li>
        <li>
            <ul class="menu">
                @foreach ($issues as $issue)
                    <li>
                        <a href="#">
                            <h3>
                                {{ $issue->title }}
                                <small class="pull-right">{{ $issue->percentage }}%</small>
                            </h3>
                            <div class="progress xs">
                                <div class="progress-bar progress-bar-aqua"
                                    style="width: {{ $issue->percentage }}%"
                                    aria-valuenow="{{ $issue->percentage }}"
                                    aria-valuemin="0" aria-valuemax="100"
                                    role="progressbar">
                                    <span class="sr-only">{{ $issue->percentage }}% Complete</span>
                                </div>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </li>
        <li class="footer">
            <a href="#">{{ __('dashboard.navbar.issues.all') }}</a>
        </li>
    </ul>
</li>
