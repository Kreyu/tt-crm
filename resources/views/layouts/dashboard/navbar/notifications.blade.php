<li class="dropdown notifications-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="far fa-bell notification-bell animated swing infinite"></i>

        @if ($notificationCount = count($notifications))
            <span class="label label-warning">{{ $notificationCount }}</span>
        @endif
    </a>
    <ul class="dropdown-menu">
        <li class="header">{{ __('dashboard.navbar.notifications.header', ['quantity' => $notificationCount]) }}</li>

        <li>
            <ul class="menu">

                @foreach ($notifications as $notification)
                    <li>
                        <a href="#">
                            <i class="fa fa-users text-aqua"></i> #
                        </a>
                    </li>
                @endforeach
            </ul>
        </li>

        <li class="footer">
            <a href="#">{{ __('dashboard.navbar.notifications.all') }}</a>
        </li>
    </ul>
</li>
