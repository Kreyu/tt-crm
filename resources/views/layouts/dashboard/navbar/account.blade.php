<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <img src="{{ asset($avatar) }}" class="user-image" />
        <span class="hidden-xs">{{ Auth::user()->name }}</span>
    </a>

    <ul class="dropdown-menu">
        <li class="user-header">
            <div class="avatar">
                <img src="{{ asset($avatar) }}" class="img-circle" />
                <div class="text"><i class="fa fa-camera"></i></div>
            </div>

            <p>
                <a href="{{ route('profile') }}">{{ Auth::user()->name }}</a>
                <small>{{ __('dashboard.navbar.account.registeredAt', [
                    'date' => Auth::user()->created_at->format('d-m-Y')]) }}</small>
            </p>
        </li>

        <!-- Menu Body -->
        {{-- <li class="user-body">
            <div class="row">
                <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                </div>
                <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                </div>
                <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                </div>
            </div>
        </li> --}}

        <!-- Menu Footer-->
        <li class="user-footer">
            <div class="pull-left">
                <a href="{{ route('profile') }}" class="btn btn-default btn-flav">
                    {{ __('dashboard.navbar.account.actions.profile') }}
                </a>
            </div>

            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                @csrf

                <div class="pull-right">
                    <button type="submit" class="btn btn-default btn-flat">
                        {{ __('dashboard.navbar.account.actions.logout') }}
                    </button>
                </div>
            </form>
        </li>
    </ul>
</li>
