<li class="dropdown timer" id="timer">
    <a href="#" class="dropdown-toggle">
        <i class="far fa-clock" style="margin-right: 3px;"></i>
        <span id="timer__value">
            {{ gmdate('H:i:s', Auth::user()->todayWorktime()) }}

            <span id="timer__value--raw" style="display: none">
                {{ Auth::user()->todayWorktime() }}
            </span>
        </span>
    </a>
</li>
