<?php

return [

    'sidebar' => [
        'search' => 'Search...',
        'dashboard' => 'Dashboard',
        'profile' => 'My profile',

        'account' => [
            'status' => [
                'online' => 'Online',
                'working' => 'Working',
            ]
        ]
    ],

    'navbar' => [
        'notifications' => [
            'header' => 'You have :quantity notifications',
            'all' => 'View all'
        ],

        'issues' => [
            'header' => 'You have :quantity assigned issues',
            'all' => 'View all'
        ],

        'account' => [
            'registeredAt' => 'Member since :date',
            'actions' => [
                'profile' => 'Profile',
                'logout' => 'Sign out'
            ]
        ]
    ],

    'footer' => [
        'version' => 'Version'
    ]

];
