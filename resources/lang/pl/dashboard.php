<?php

return [

    'sidebar' => [
        'search' => 'Szukaj...',
        'dashboard' => 'Pulpit',
        'profile' => 'Mój profil',

        'account' => [
            'status' => [
                'online' => 'Online',
                'working' => 'Pracuje',
            ]
        ]
    ],

    'navbar' => [
        'notifications' => [
            'header' => 'Masz :quantity powiadomień',
            'all' => 'Wyświetl'
        ],

        'issues' => [
            'header' => 'Masz :quantity przypisanych zadagnień',
            'all' => 'Wyświetl'
        ],

        'account' => [
            'registeredAt' => 'Członek od :date',

            'actions' => [
                'profile' => 'Profil',
                'logout' => 'Wyloguj'
            ]
        ]
    ],

    'footer' => [
        'version' => 'Wersja'
    ]

];
