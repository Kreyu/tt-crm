<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Scopes\ProjectScopes;

/**
 * Project model
 *
 * Model representing single entity from projects table.
 *
 * @package App\Models
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
final class Project extends Model
{
    use ProjectScopes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    /**
     * Get the issues for the project.
     *
     * @return HasMany
     */
    public function issues(): HasMany
    {
        return $this->hasMany(Issue::class);
    }
}
