<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Scopes\CommentScopes;

/**
 * Comment model
 *
 * Model representing single entity from comments table.
 *
 * @package App\Models
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
final class Comment extends Model
{
    use CommentScopes;

    /**
     * Get the issue that contains the comment.
     *
     * @return BelongsTo
     */
    public function issue(): BelongsTo
    {
        return $this->belongsTo(Issue::class);
    }

    /**
     * Get the user that created the comment.
     *
     * @return BelongsTo
     */
    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
