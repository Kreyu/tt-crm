<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Notifications\Notifiable;
use App\Scopes\UserScopes;

/**
 * User model
 *
 * Model representing single entity from users table.
 *
 * @package App\Models
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
final class User extends Authenticatable
{
    use UserScopes, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
        'enabled', 'accepted', 'is_working',
        'signed_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the issues that were created by the user.
     *
     * @return HasMany
     */
    public function createdIssues(): HasMany
    {
        return $this->hasMany(Issue::class, 'creator_id');
    }

    /**
     * Get the assigned issues for the user.
     *
     * @return HasMany
     */
    public function assignedIssues(): HasMany
    {
        return $this->hasMany(Issue::class, 'assignee_id');
    }

    /**
     * Get the comments created by the user.
     *
     * @return HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class, 'author_id');
    }

    /**
     * Get the timesheets for the user.
     *
     * @return HasMany
     */
    public function timesheets(): HasMany
    {
        return $this->hasMany(Timesheet::class);
    }

    /**
     * Get the files created by the user.
     *
     * @return HasMany
     */
    public function files(): HasMany
    {
        return $this->hasMany(File::class);
    }

    /**
     * Get the notifications for the user.
     *
     * @return HasMany
     */
    public function notifications(): HasMany
    {
        return $this->hasMany(Notification::class);
    }

    /**
     * Get user's avatar file entry.
     *
     * @return null|File
     */
    public function avatar(): ?File
    {
        return $this->files()->avatars()
            ->latest()->first();
    }

    /**
     * Get user's avatar path. Return placeholder path if null.
     *
     * @return string
     */
    public function avatarFullPath(): string
    {
        return optional($this->avatar())->fullPath()
            ?? File::AVATAR_PLACEHOLDER;
    }

    /**
     * Get the read notifications for the user.
     *
     * @return HasMany
     */
    public function readNotifications(): HasMany
    {
        return $this->notifications()->read();
    }

    /**
     * Get the unread notifications for the user.
     *
     * @return HasMany
     */
    public function unreadNotifications(): HasMany
    {
        return $this->notifications()->unread();
    }

    /**
     * Sets accepted property to true.
     *
     * @return self
     */
    public function accept(): self
    {
        $this->accepted = true;

        return $this;
    }

    /**
     * Determine if the user is accepted.
     *
     * @return boolean
     */
    public function isAccepted(): bool
    {
        return $this->accepted;
    }

    /**
     * Determine if the user is enabled.
     *
     * @return boolean
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * Determine if the user is working.
     *
     * @return boolean
     */
    public function isWorking(): bool
    {
        return $this->is_working;
    }

    /**
     * Sign in the user, setting is_working property to true,
     * and signed_at property to current date.
     *
     * @return self
     */
    public function signIn(): self
    {
        $this->is_working = true;
        $this->signed_at = now();

        return $this;
    }

    /**
     * Sign in the user, setting is_working property to false.
     *
     * @return self
     */
    public function signOut(): self
    {
        $this->is_working = false;

        return $this;
    }

    /**
     * Get all the timesheets from today for the user.
     *
     * @return Collection
     */
    public function todayTimesheets(): Collection
    {
        return $this->timesheets()->fromToday()->get();
    }

    /**
     * Get today worktime in seconds for the user.
     *
     * @return integer
     */
    public function todayWorktime(): int
    {
        return (int) $this->timesheets()->fromToday()->sum('time_elapsed');
    }

    /**
     * Get overall worktime in seconds for the user.
     *
     * @return integer
     */
    public function overallWorktime(): int
    {
        return (int) $this->timesheets()->sumTime();
    }
}
