<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\FileScopes;
use App\Enums\FileType;

/**
 * File model
 *
 * Model representing single entity from files table.
 *
 * @package App\Models
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
final class File extends Model
{
    use FileScopes;

    const PATH_ATTACHMENT = 'attachments';
    const PATH_AVATAR = 'avatars';
    const AVATAR_PLACEHOLDER = 'img/avatar_placeholder.png';

    /**
     * Get the user that this file belongs to.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the project that this file belongs to.
     *
     * @return BelongsTo
     */
    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * Get the issue that this file belongs to.
     *
     * @return BelongsTo
     */
    public function issue(): BelongsTo
    {
        return $this->belongsTo(Issue::class);
    }

    /**
     * Get full path of the file.
     *
     * @return string
     */
    public function fullPath(): string
    {
        return $this->path() . '/' . $this->filename . '.' . $this->extension;
    }

    /**
     * Get path of the file, without filename nor extension.
     *
     * @return string
     */
    public function path(): string
    {
        $type = FileType::getKey($this->type);

        return 'storage/' . constant(self::class . "::PATH_$type");
    }
}
