<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Scopes\IssueScopes;

/**
 * Issue model
 *
 * Model representing single entity from issues table.
 *
 * @package App\Models
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
final class Issue extends Model
{
    use IssueScopes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description',
        'project_id', 'creator_id', 'assignee_id',
        'type', 'status', 'priority', 'percentage',
        'start_date', 'due_date'
    ];

    /**
     * Get the user assigned to the issue.
     *
     * @return BelongsTo
     */
    public function assignee(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the user that created the issue.
     *
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the project that contains the issue.
     *
     * @return BelongsTo
     */
    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * Get the comments for the issue.
     *
     * @return HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }
}
