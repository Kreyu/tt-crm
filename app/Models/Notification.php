<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Scopes\NotificationScopes;

/**
 * Notification model
 *
 * Model representing single entity from notifications table.
 *
 * @package App\Models
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
final class Notification extends Model
{
    use NotificationScopes;

    /**
     * Get the user that this notification belongs to.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the project for the notification.
     *
     * @return HasOne
     */
    public function project(): HasOne
    {
        return $this->hasOne(Project::class);
    }

    /**
     * Get the issue for the notification.
     *
     * @return HasOne
     */
    public function issue(): HasOne
    {
        return $this->hasOne(Issue::class);
    }

    /**
     * Mark notification as read.
     *
     * @return self
     */
    public function markRead(): self
    {
        $this->is_read = true;

        return $this;
    }

    /**
     * Mark notification as unread.
     *
     * @return self
     */
    public function markUnread(): self
    {
        $this->is_read = false;

        return $this;
    }
}
