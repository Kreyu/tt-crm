<?php

namespace App\Http\Middleware;

use Closure;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->get('token');

        if ($sessionUser = $request->session()->get('user')) {
            if ($sessionUser->api_token !== $token) {
                return response('Unauthorized.', 401)->json();
            }

            return $next($request);
        }

        $user = \App\Models\User::withApiToken($token)->first();

        if (!$user) {
            return response('Unauthorized.', 401)->json();
        }

        $request->session()->put('user', $user);

        return $next($request);
    }
}
