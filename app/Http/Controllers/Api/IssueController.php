<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\IssueCollection;
use App\Http\Resources\IssueResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use App\Services\Signer;
use App\Models\Issue;

/**
 * Issue controller
 *
 * This controller handles API requests for Issue entries.
 *
 * @package App\Http\Controllers\Api
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
class IssueController extends ApiController
{
    /**
     * Create a new issue controller.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show collection of Issue entries
     *
     * @return IssueCollection
     */
    public function index(): IssueCollection
    {
        return new IssueCollection(Issue::all());
    }

    /**
     * Show resource of Issue entry
     *
     * @param  Issue $issue
     * @return IssueResource
     */
    public function show(Issue $issue): IssueResource
    {
        return new IssueResource($issue);
    }
}
