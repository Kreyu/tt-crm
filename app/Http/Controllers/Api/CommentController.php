<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CommentCollection;
use App\Http\Resources\CommentResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use App\Services\Signer;
use App\Models\Comment;

/**
 * Comment controller
 *
 * This controller handles API requests for Comment entries.
 *
 * @package App\Http\Controllers\Api
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
class CommentController extends ApiController
{
    /**
     * Create a new comment controller.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show collection of Comment entries
     *
     * @return CommentCollection
     */
    public function index(): CommentCollection
    {
        return new CommentCollection(Comment::all());
    }

    /**
     * Show resource of Comment entry
     *
     * @param  Comment $comment
     * @return CommentResource
     */
    public function show(Comment $comment): CommentResource
    {
        return new CommentResource($comment);
    }
}
