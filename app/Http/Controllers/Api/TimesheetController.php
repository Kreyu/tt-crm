<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\TimesheetCollection;
use App\Http\Resources\TimesheetResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use App\Services\Signer;
use App\Models\Timesheet;

/**
 * Timesheet controller
 *
 * This controller handles API requests for Timesheet entries.
 *
 * @package App\Http\Controllers\Api
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
class TimesheetController extends ApiController
{
    /**
     * Create a new timesheet controller.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'worktime']);
    }

    /**
     * Show collection of Timesheet entries
     *
     * @return TimesheetCollection
     */
    public function index(): TimesheetCollection
    {
        return new TimesheetCollection(Timesheet::all());
    }

    /**
     * Show resource of Timesheet entry
     *
     * @param  Timesheet $timesheet
     * @return TimesheetResource
     */
    public function show(Timesheet $timesheet): TimesheetResource
    {
        return new TimesheetResource($timesheet);
    }

    public function worktime()
    {
        return response()->json(100);
    }
}
