<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use App\Services\Signer;
use App\Models\User;

/**
 * User controller
 *
 * This controller handles API requests for User entries.
 *
 * @package App\Http\Controllers\Api
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
class UserController extends ApiController
{
    /**
     * Show collection of User entries
     *
     * @return UserCollection
     */
    public function index(): UserCollection
    {
        return new UserCollection(User::all());
    }

    /**
     * Show resource of User entry
     *
     * @param  User $user
     * @return UserResource
     */
    public function show(User $user): UserResource
    {
        return new UserResource($user);
    }

    public function worktime(Request $request)
    {
        return response()->json(
            $request->session()->get('user')->todayWorktime()
        );
    }

    public function sign(Request $request)
    {
        Signer::sign(
            $request->session()->get('user')
        );

        return response()->json('OK');
    }
}
