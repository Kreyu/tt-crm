<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ProjectCollection;
use App\Http\Resources\ProjectResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use App\Services\Signer;
use App\Models\Project;

/**
 * Project controller
 *
 * This controller handles API requests for Project entries.
 *
 * @package App\Http\Controllers\Api
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
class ProjectController extends ApiController
{
    /**
     * Create a new project controller.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show collection of Project entries
     *
     * @return ProjectCollection
     */
    public function index(): ProjectCollection
    {
        return new ProjectCollection(Project::all());
    }

    /**
     * Show resource of Project entry
     *
     * @param  Project $project
     * @return ProjectResource
     */
    public function show(Project $project): ProjectResource
    {
        return new ProjectResource($project);
    }
}
