<?php

namespace App\Http\Controllers\Web;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * Web controller
 *
 * This controller handles web requests.
 *
 * @package App\Http\Controllers\Web
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
class WebController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
