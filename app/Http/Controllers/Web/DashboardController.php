<?php

namespace App\Http\Controllers\Web;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Services\Signer;
use App\Models\File;
use Illuminate\Support\Facades\App;

/**
 * Dashboard controller
 *
 * This controller handles web requests related with dashboard.
 *
 * @package App\Http\Controllers\Web
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
class DashboardController extends WebController
{
    /**
     * Create a new dashboard controller.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->shareVariables();
    }

    /**
     * Show dashboard view.
     *
     * @return \Illuminate\View\View
     */
    public function dashboard(): View
    {
        return view('dashboard.dashboard');
    }

    /**
     * Show profile view.
     *
     * @return \Illuminate\View\View
     */
    public function profile(): View
    {
        return view('dashboard.profile', [
            'createdIssues' => Auth::user()->createdIssues()->count(),
            'assignedIssues' => Auth::user()->assignedIssues()->count()
        ]);
    }

    /**
     * Share variables to every dashboard view.
     *
     * @return void
     */
    private function shareVariables(): void
    {
        $this->middleware(function ($request, $next) {
            $avatar = Auth::user()->avatarFullPath();
            $issues = Auth::user()->assignedIssues()->get();
            $notifications = Auth::user()->unreadNotifications()->get();

            view()->share('issues', $issues);
            view()->share('avatar', $avatar);
            view()->share('notifications', $notifications);

            return $next($request);
        });
    }
}
