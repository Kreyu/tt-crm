<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Issue resource
 *
 * Resource for API requests with Issue entity.
 *
 * @package App\Http\Resources
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
class IssueResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
