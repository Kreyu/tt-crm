<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Comment collection
 *
 * Resource collection for API requests with Comment entries.
 *
 * @package App\Http\Resources
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
class CommentCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection
        ];
    }
}
