<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Project collection
 *
 * Resource collection for API requests with Project entries.
 *
 * @package App\Http\Resources
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
class ProjectCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection
        ];
    }
}
