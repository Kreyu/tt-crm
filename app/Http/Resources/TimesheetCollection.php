<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Timesheet collection
 *
 * Resource collection for API requests with Timesheet entries.
 *
 * @package App\Http\Resources
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
class TimesheetCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection
        ];
    }
}
