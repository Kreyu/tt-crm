<?php

namespace App\Services;

use Illuminate\Support\Carbon;
use App\Events\UserSignedOut;
use App\Events\UserSignedIn;
use App\Events\UserSigned;
use App\Models\Timesheet;
use App\Models\User;

/**
 * Signer service
 *
 * Service with static methods for signing in/out the users.
 *
 * @package App\Services
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
final class Signer
{
    /**
     * Signs given user.
     *
     * @param  User $user
     * @return void
     */
    public static function sign(User $user)
    {
        $user->isWorking() ?
            static::signOut($user) :
            static::signIn($user);

        event(new UserSigned($user));
    }

    /**
     * Signs in given user.
     *
     * @param  User $user
     * @return void
     */
    public static function signIn(User $user)
    {
        $user->signIn()->save();

        event(new UserSignedIn($user));
    }

    /**
     * Signs out given user.
     *
     * @param  User $user
     * @return void
     */
    public static function signOut(User $user)
    {
        $user->signOut()->save();

        Timesheet::create([
            'user_id' => $user->id,
            'time_elapsed' => now()->diffInSeconds($user->signed_at)
        ]);

        event(new UserSignedOut($user));
    }
}
