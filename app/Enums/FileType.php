<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * FileType enum
 *
 * An enum class holding all the file entity types as constants.
 *
 * @package App\Enums
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
final class FileType extends Enum
{
    const ATTACHMENT = 0;
    const AVATAR = 1;
}
