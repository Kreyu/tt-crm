<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * IssueType enum
 *
 * An enum class holding all the issue types as constants.
 *
 * @package App\Enums
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
final class IssueType extends Enum
{
    const BUG = 0;
    const FEATURE = 1;
    const SUPPORT = 2;
}
