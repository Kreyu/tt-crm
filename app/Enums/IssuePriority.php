<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * IssuePriority enum
 *
 * An enum class holding all the issue priorities as constants.
 *
 * @package App\Enums
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
final class IssuePriority extends Enum
{
    const LOW = 0;
    const NORMAL = 1;
    const HIGH = 2;
    const URGENT = 3;
    const IMMEDIATE = 4;
}
