<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * IssueStatus enum
 *
 * An enum class holding all the issue statuses as constants.
 *
 * @package App\Enums
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
final class IssueStatus extends Enum
{
    const NEW = 0;
    const ASSIGNED = 1;
    const RESOLVED = 2;
    const FEEDBACK = 3;
    const CLOSED = 4;
    const REJECTED = 5;
}
