<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;

class UserAcceptation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:accept {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Accepts user with given email.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $user = User::byEmail($email)->first();

        if (!$user) {
            return $this->warn('User with given email does not exist.');
        }

        if ($user->isAccepted()) {
            return $this->warn('User with given email is already accepted.');
        }

        $user->accept();
        $success = $user->save();

        return $success ? 
            $this->info('User successfully accepted.') :
            $this->error('An error occured while updating user.');
    }
}
