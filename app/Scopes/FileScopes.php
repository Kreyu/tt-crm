<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use App\Scopes\Versatile\TimestampScopes;
use App\Enums\FileType;
use App\Models\User;

/**
 * File scopes
 *
 * Local scopes trait to use within File model.
 *
 * @package App\Scopes
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
trait FileScopes
{
    use TimestampScopes;

    public function scopeAttachments(): Builder
    {
        return $query->where('type', FileType::getValue('ATTACHMENT'));
    }

    public function scopeAvatars(Builder $query): Builder
    {
        return $query->where('type', FileType::getValue('AVATAR'));
    }

    public function scopeCreatedBy(Builder $query, User $user): Builder
    {
        return $query->where('user_id', $user->id);
    }
}