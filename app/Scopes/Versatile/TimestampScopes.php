<?php

namespace App\Scopes\Versatile;

use Illuminate\Database\Eloquent\Builder;

/**
 * Timestamp scopes
 *
 * Local scopes trait to use within models with timestamps.
 *
 * @package App\Scopes\Versatile
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
trait TimestampScopes
{
    /**
     * Scope a query to only include entries from given date.
     *
     * @param  Builder   $query
     * @param  \DateTime $date
     * @return Builder
     */
    public function scopeFrom(Builder $query, \DateTime $date): Builder
    {
        return $query->where('created_at', $date);
    }

    /**
     * Scope a query to only include entries from given day.
     *
     * @param  Builder   $query
     * @param  \DateTime $date
     * @return Builder
     */
    public function scopeFromDay(Builder $query, \DateTime $date): Builder
    {
        return $query->whereDay('created_at', $date);
    }

    /**
     * Scope a query to only include entries from given month.
     *
     * @param  Builder   $query
     * @param  \DateTime $date
     * @return Builder
     */
    public function scopeFromMonth(Builder $query, \DateTime $date): Builder
    {
        return $query->whereMonth('created_at', $date->format('m'));
    }

    /**
     * Scope a query to only include entries from given year.
     *
     * @param  Builder   $query
     * @param  \DateTime $date
     * @return Builder
     */
    public function scopeFromYear(Builder $query, \DateTime $date): Builder
    {
        return $query->whereYear('created_at', $date->format('Y'));
    }

    /**
     * Scope a query to only include entries from actual day.
     *
     * @param  Builder $query
     * @return Builder
     */
    public function scopeFromToday(Builder $query): Builder
    {
        return $query->whereDay('created_at', today()->format('d'));
    }

    /**
     * Scope a query to only include entries since given date.
     *
     * @param  Builder   $query
     * @param  \DateTime $date
     * @return Builder
     */
    public function scopeSince(Builder $query, \DateTime $date): Builder
    {
        return $query->where('created_at', '>', $date);
    }

    /**
     * Scope a query to only include entries until given date.
     *
     * @param  Builder   $query
     * @param  \DateTime $date
     * @return Builder
     */
    public function scopeUntil(Builder $query, \DateTime $date): Builder
    {
        return $query->where('created_at', '<', $date);
    }
}
