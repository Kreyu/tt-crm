<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use App\Scopes\Versatile\TimestampScopes;

/**
 * Notification scopes
 *
 * Local scopes trait to use within Notification model.
 *
 * @package App\Scopes
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
trait NotificationScopes
{
    use TimestampScopes;

    public function scopeRead(Builder $query): Builder
    {
        return $query->where('is_read', true);
    }

    public function scopeUnread(Builder $query): Builder
    {
        return $query->where('is_read', false);
    }
}
