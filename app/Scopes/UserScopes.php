<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use App\Scopes\Versatile\TimestampScopes;

/**
 * User scopes
 *
 * Local scopes trait to use within User model.
 *
 * @package App\Scopes
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
trait UserScopes
{
    use TimestampScopes;

    /**
     * Scope a query to only include entries with given api token.
     *
     * @param  Builder $query
     * @param  string  $token
     * @return integer
     */
    public function scopeWithApiToken(Builder $query, string $token): Builder
    {
        return $query->where('api_token', $token);
    }

    /**
     * Scope a query to only include entries with given email.
     *
     * @param  Builder $query
     * @param  string  $email
     * @return integer
     */
    public function scopeWithEmail(Builder $query, string $email): Builder
    {
        return $query->where('email', $email);
    }

    /**
     * Scope a query to only include entries with given name.
     *
     * @param  Builder $query
     * @param  string  $name
     * @return integer
     */
    public function scopeWithName(Builder $query, string $name): Builder
    {
        return $query->where('name', $name);
    }

    /**
     * Scope a query to only include enabled entries.
     *
     * @param  Builder $query
     * @return integer
     */
    public function scopeEnabled(Builder $query): Builder
    {
        return $query->where('enabled', true);
    }

    /**
     * Scope a query to only include accepted entries.
     *
     * @param  Builder $query
     * @return integer
     */
    public function scopeAccepted(Builder $query): Builder
    {
        return $query->where('accepted', true);
    }

    /**
     * Scope a query to only include working entries.
     *
     * @param  Builder $query
     * @return integer
     */
    public function scopeCurrentlyWorking(Builder $query): Builder
    {
        return $query->where('is_working', true);
    }

    /**
     * Scope a query to only include entries with given sign date.
     *
     * @param  Builder  $query
     * @param  \DateTime $signedAt
     * @return integer
     */
    public function scopeSignedAt(Builder $query, \DateTime $signedAt): Builder
    {
        return $query->where('signed_at', $signedAt);
    }
}
