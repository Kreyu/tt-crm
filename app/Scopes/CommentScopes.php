<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use App\Scopes\Versatile\TimestampScopes;
use App\Models\User;
use App\Models\Issue;

/**
 * Comment scopes
 *
 * Local scopes trait to use within Comment model.
 *
 * @package App\Scopes
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
trait CommentScopes
{
    use TimestampScopes;

    /**
     * Scope a query to only include entries with given author user.
     *
     * @param  Builder  $query
     * @param  User     $user
     * @return Builder
     */
    public function scopeCreatedBy(Builder $query, User $user): Builder
    {
        return $query->where('author_id', $user->id);
    }

    /**
     * Scope a query to only include entries related with given issue.
     *
     * @param  Builder  $query
     * @param  Issue    $issue
     * @return Builder
     */
    public function scopeForIssue(Builder $query, Issue $issue): Builder
    {
        return $query->where('issue_id', $issue->id);
    }
}
