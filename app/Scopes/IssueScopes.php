<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use App\Scopes\Versatile\TimestampScopes;

/**
 * Issue scopes
 *
 * Local scopes trait to use within Issue model.
 *
 * @package App\Scopes
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
trait IssueScopes
{
    use TimestampScopes;

    /**
     * Scope a query to only include entries with given creator.
     *
     * @param  Builder  $query
     * @param  User     $user
     * @return Builder
     */
    public function scopeCreatedBy(Builder $query, User $user): Builder
    {
        return $query->where('creator_id', $user->id);
    }

    /**
     * Scope a query to only include entries with given assignee.
     *
     * @param  Builder  $query
     * @param  string   $title
     * @return Builder
     */
    public function scopeAssignedTo(Builder $query, User $user): Builder
    {
        return $query->where('assignee_id', $user->id);
    }
}
