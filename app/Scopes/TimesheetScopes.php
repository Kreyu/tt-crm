<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use App\Scopes\Versatile\TimestampScopes;
use App\Models\User;

/**
 * Timesheet scopes
 *
 * Local scopes trait to use within Timesheet model.
 *
 * @package App\Scopes
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
trait TimesheetScopes
{
    use TimestampScopes;

    /**
     * Scope a query to only include entries with given user.
     *
     * @param  Builder  $query
     * @param  User     $user
     * @return Builder
     */
    public function scopeFor(Builder $query, User $user): Builder
    {
        return $query->where('user_id', $user->id);
    }

    /**
     * Scope a query to sum time_elapsed column.
     *
     * @param  Builder $query
     * @return integer
     */
    public function scopeSumTime(Builder $query): int
    {
        return $query->sum('time_elapsed');
    }
}
