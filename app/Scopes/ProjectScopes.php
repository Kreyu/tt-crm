<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use App\Scopes\Versatile\TimestampScopes;

/**
 * Project scopes
 *
 * Local scopes trait to use within Project model.
 *
 * @package App\Scopes
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 */
trait ProjectScopes
{
    use TimestampScopes;

    /**
     * Scope a query to only include entries with given name.
     *
     * @param  Builder  $query
     * @param  User     $user
     * @return Builder
     */
    public function scopeNamed(Builder $query, string $name): Builder
    {
        return $query->where('name', $name);
    }
}
