<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(User::class, 5)
            ->make()->each(function (User $user) {
                $user->makeVisible('password');
                $user->password = bcrypt('secret');
                $user->created_at = now();
                $user->updated_at = now();
            })->toArray();

        User::insert($users);
    }
}
