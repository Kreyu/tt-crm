<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(IssuesTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
        $this->call(TimesheetsTableSeeder::class);
        $this->call(NotificationsTableSeeder::class);
        $this->call(FilesTableSeeder::class);
    }
}
