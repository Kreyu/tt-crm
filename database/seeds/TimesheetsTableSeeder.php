<?php

use Illuminate\Database\Seeder;
use App\Models\Timesheet;

class TimesheetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Timesheet::class, 5)->create();
    }
}
