<?php

use Faker\Generator as Faker;
use App\Models\Project;
use App\Models\Issue;
use App\Models\File;
use App\Models\User;
use App\Enums\FileType;

$factory->define(File::class, function (Faker $faker) {
    return [
        'user_id' => $faker->randomElement(User::pluck('id')->toArray()),
        'issue_id' => $faker->randomElement(Issue::pluck('id')->toArray()),
        'project_id' => $faker->randomElement(Project::pluck('id')->toArray()),
        'type' => FileType::getValue('ATTACHMENT'),
        'filename' => 'placeholder/' . rand(1, 5),
        'extension' => 'jpg'
    ];
});
