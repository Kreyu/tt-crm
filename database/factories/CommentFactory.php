<?php

use Faker\Generator as Faker;
use App\Models\Comment;
use App\Models\Issue;
use App\Models\User;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'author_id' => $faker->randomElement(User::pluck('id')->toArray()),
        'issue_id' => $faker->randomElement(Issue::pluck('id')->toArray()),
        'content' => $faker->sentence(25)
    ];
});
