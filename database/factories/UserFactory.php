<?php

use Faker\Generator as Faker;
use App\Models\User;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'remember_token' => str_random(10),
        'accepted' => false,
        'enabled' => true,
        'is_working' => false,
        'signed_at' => null,
        'api_token' => str_random(32)
    ];
});
