<?php

use Faker\Generator as Faker;
use App\Models\Notification;
use App\Models\User;
use App\Models\Project;
use App\Models\Issue;

$factory->define(Notification::class, function (Faker $faker) {
    return [
        'user_id' => $faker->randomElement(User::pluck('id')->toArray()),
        'issue_id' => $faker->randomElement(Issue::pluck('id')->toArray()),
        'project_id' => $faker->randomElement(Project::pluck('id')->toArray()),
        'is_read' => false,
        'type' => rand(0, 3),
    ];
});
