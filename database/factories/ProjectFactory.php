<?php

use Faker\Generator as Faker;
use App\Models\Project;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'description' => $faker->sentence(),
    ];
});
