<?php

use Faker\Generator as Faker;
use App\Models\Timesheet;
use App\Models\User;

$factory->define(Timesheet::class, function (Faker $faker) {
    return [
        'user_id' => $faker->randomElement(User::pluck('id')->toArray()),
        'time_elapsed' => rand(0, 3600)
    ];
});
