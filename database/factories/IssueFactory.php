<?php

use Faker\Generator as Faker;
use App\Enums\IssuePriority;
use App\Enums\IssueStatus;
use App\Enums\IssueType;
use App\Models\Project;
use App\Models\Issue;
use App\Models\User;

$factory->define(Issue::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(3),
        'description' => $faker->sentence(8),

        'creator_id' => $faker->randomElement(User::pluck('id')->toArray()),
        'assignee_id' => $faker->randomElement(User::pluck('id')->toArray()),
        'project_id' => $faker->randomElement(Project::pluck('id')->toArray()),

        'type' => IssueType::getRandomValue(),
        'status' => IssueStatus::getRandomValue(),
        'priority' => IssuePriority::getRandomValue(),

        'percentage' => rand(0, 100),
        'start_date' => $faker->dateTimeThisYear(),
        'due_date' => $faker->dateTimeThisYear()
    ];
});
