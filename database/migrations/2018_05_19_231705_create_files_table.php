<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->unsignedInteger('issue_id')->nullable();
            $table->unsignedInteger('project_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('issue_id')->references('id')->on('issues');
            $table->foreign('project_id')->references('id')->on('projects');
            $table->tinyInteger('type');
            $table->string('filename');
            $table->string('extension');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
