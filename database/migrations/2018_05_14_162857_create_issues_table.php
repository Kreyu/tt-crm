<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issues', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title');
            $table->text('description');
            $table->unsignedInteger('project_id');
            $table->unsignedInteger('creator_id');
            $table->unsignedInteger('assignee_id');
            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('creator_id')->references('id')->on('users');
            $table->foreign('assignee_id')->references('id')->on('users');
            $table->tinyInteger('type');
            $table->tinyInteger('status');
            $table->tinyInteger('priority');
            $table->tinyInteger('percentage');
            $table->datetime('start_date');
            $table->datetime('due_date');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
