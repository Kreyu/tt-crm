# tt-crm
CRM system with time tracking. Inspired by Redmine, created for fun.

```bash
# To run project
composer install
php artisan migrate
php artisan db:seed
php artisan lang:js --json
```
