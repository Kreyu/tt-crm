const { mix } = require('laravel-mix');

mix.webpackConfig({
    resolve: {
        extensions: ['.ts']
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                loader: 'ts-loader'
            }
        ]
    }
});

mix.js('resources/assets/ts/dashboard.ts', 'public/js')
    .sass('resources/assets/sass/auth.scss', 'public/css')
    .sass('resources/assets/sass/dashboard.scss', 'public/css');

if (mix.inProduction()) {
    mix.version();
}
